Program matematicas;

uses crt;

VAR

	r, a:integer; 
	ar,v:real;
	

begin
	
	writeln ('           Programa para calcular el area y el volumen de un cilindro');
	
	writeln(' ');
	writeln(' ');
	
	writeln ('Digite el valor del radio: ');
	readln (r);
	
	writeln ('Digite el valor de la altura: ');
	readln (a);
	
	ar:=2*pi*r*a;
	writeln(' ');
	
	writeln (' El area es: ', ar:0:0);
	v:=pi*a*sqr(r);
	writeln (' El volumen es: ', v:0:0);
	
	readkey;


	end.


Program trigonometricas;

uses crt; 

	
VAR	

	dato, data, sen, opn:real; 
	
	
	
	begin
	
	clrscr;
	
	writeln ('        ..... > MENU < .....');
	
	writeln(' ');
	
	writeln ('Por favor digite una opcion');
	
	writeln(' ');
	
	writeln('1: Calculo del seno.');
	
	writeln ('2: Calculo del coseno.');
	
	writeln ('3: Salir.');
	
	
	readln(opn);
	clrscr;
	

	if(opn=1) then
	
	begin
	
	writeln ('Digite el numero del seno a calcular:');
	readln (dato);
	data:=dato*pi/180;
	sen:=sin(data);
	writeln ('El valor del seno es: ', sen: 8:9);
	
	readkey;
	
	end;


	if(opn=2) then 


	BEGIN

	writeln ('Digite el numero del coseno a calcular:');
	readln (dato);
	data:=dato*pi/180;
	sen:=cos(data);
	writeln ('El valor del cos es: ', sen: 8:9);

	readkey;

	end;
	

	if(opn=3) then
	begin
	
	writeln('Gracias por usar este programa:)');
	readkey;
	

	
	end;
end.

Program ordinarias;

uses crt; 

	
VAR	

	 a, b, c, y, x: integer;
	

	begin

	writeln ('      Digite 3 datos enteros que represente 3 coeficientes de un polinomio');
	writeln(' ');
	writeln(' ');
	writeln('Digite numero 1: ');
	readln (c);
	writeln(' ');
	writeln('Digite numero 2: ');
	readln (a);
	writeln(' ');
	writeln('Digite numero 3: ');
	writeln(' ');
	readln (b);
	
	clrscr;
	writeln ('El polinomio es: ', b, 'X^2+' , a, 'x+' , c);
	writeln(' ');
	
	writeln ('Coloque un valor entero para x: ');
	readln (x);
	writeln(' ');
	
	y :=b * sqr(x) + a * x +c;
	writeln ('El valor f(x): ( ', x, ') es: ', y);
	
	readkey;
	
	

end.


program Especiales;

uses crt;

var radio,resultado:real;

const pi=3.14;


	begin

	clrscr;
	writeln ('       ..... > Calculador del area de una circunferencia < .....');
	writeln (' ');
	writeln (' ');
	write('Introduce el radio de la circunferencia:');

	read(radio);

	resultado:=pi*(radio*radio);
	writeln (' ');
	write('El area de la circunferencia es: ',resultado:5:2);
	
	writeln (' ');
	writeln (' ');
	readkey;
	writeln('Gracias por usar este programa :)');
	readkey;
	
	
	
	
	
end.
